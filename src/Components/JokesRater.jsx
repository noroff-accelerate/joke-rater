import { useEffect, useState } from "react";
import Joke from "./Joke";
import { useDispatch, useSelector } from "react-redux";
import { addToMeh, addToRofl } from "../Slices/JokeRatesSlice";

function JokeRater() {
  const [currentJoke, setCurrentJoke] = useState({});
  const dispatch = useDispatch();
  const ratedJokes = useSelector(state=> state.ratedJokes)
  const user = useSelector(state=> state.user)

  useEffect(() => {
    nextJoke()
  }, []);

  function upVote(){
        dispatch(addToRofl(currentJoke)) 
        nextJoke()
  }

  function downVote(){
        dispatch(addToMeh(currentJoke)) 
        nextJoke()
  }

  function nextJoke(){
    fetch(
        "https://v2.jokeapi.dev/joke/Any?blacklistFlags=nsfw,religious,political,racist,sexist,explicit&type=single"
      )
        .then((response) => response.json())
        .then((result) => setCurrentJoke(result));
  }

  return (
    <div>
      <h4>Welcome {user.username}</h4>
      <Joke joke={currentJoke}></Joke>
      <button onClick={upVote}>🤣</button>
      <button onClick={downVote}>😐</button>
      <p>Rofl count</p>{ratedJokes.rofl.length}
      <p>Meh count</p>{ratedJokes.meh.length}
    </div>
  );
}
export default JokeRater;
