import { useSelector } from "react-redux"

function JokeRateReport(){
    const ratedJokes = useSelector(state=> state.ratedJokes)
    const user = useSelector(state=> state.user)
    return (<div>
         <h4>Welcome {user.username}</h4>
        <p>Rofl count</p>{ratedJokes.rofl.length}
      <p>Meh count</p>{ratedJokes.meh.length}
    </div>)
}

export default JokeRateReport