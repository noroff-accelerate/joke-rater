import { useDispatch, useSelector } from "react-redux";
import { getSampleUser } from "../Slices/userSlice";
import { useNavigate } from "react-router-dom";

function LoginForm() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
 

  function fetchSampleUser() {
    dispatch(getSampleUser())
    navigate('/ratejokes')
  }

  return (
    <div>
      <p>Login</p>
      <button onClick={fetchSampleUser}>Login with sample user</button>
    </div>
  );
}

export default LoginForm;
