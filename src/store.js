import { configureStore } from "@reduxjs/toolkit";
import jokeRatingsReducer from "./Slices/JokeRatesSlice"
import userReducer from "./Slices/userSlice";


export default configureStore({
    reducer:{
        ratedJokes:jokeRatingsReducer,
        user:userReducer
    }
})