import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter, NavLink, Route, Routes } from "react-router-dom";
import LoginPage from "./Pages/LoginPage";
import JokeRatePage from "./Pages/JokeRatePage";
import RatingReportPage from "./Pages/RatingReportPage";

function App() {
  return (
    <div>
    
      <BrowserRouter>
      <NavLink to="/">Login</NavLink>
      <NavLink to="/ratejokes">Jokes</NavLink>
      <NavLink to="/ratereport">Report</NavLink>
        <Routes>
          <Route path="/" element={<LoginPage></LoginPage>} />
          <Route path="/ratejokes" element={<JokeRatePage></JokeRatePage>} />
          <Route
            path="/ratereport"
            element={<RatingReportPage></RatingReportPage>}
          />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
