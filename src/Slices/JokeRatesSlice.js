import { createSlice } from "@reduxjs/toolkit";

export const jokeRatingsSlice = createSlice({
  name: "jokeRatings",
  initialState: {
    rofl: [],
    meh: [],
  },
  reducers: {
    addToRofl: (state, jokePayload) => {
      state.rofl.push(jokePayload);
    },
    addToMeh: (state, jokePayload) => {
      state.meh.push(jokePayload);
    },
  },
});

export const {addToRofl, addToMeh} = jokeRatingsSlice.actions

export default jokeRatingsSlice.reducer