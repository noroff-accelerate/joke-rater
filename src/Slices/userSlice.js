import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

export const getSampleUser = createAsyncThunk(
  "user/getSampleUser",
  async () => {
    const reponse = await fetch("https://randomuser.me/api/");
    const result = await reponse.json();
    return result;
  }
);

export const userSlice = createSlice({
  name: "user",
  initialState: {
    username: "none",
    email: "none",
  },
  reducers: {},
  extraReducers: {
    // set user from api
    [getSampleUser.fulfilled]: (state, action) => {
      console.log(action);
      return {
        username: action.payload.results[0].login.username,
        email: action.payload.results[0].email,
      };
    },
  },
});

export default userSlice.reducer;
